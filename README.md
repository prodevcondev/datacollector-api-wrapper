DataCollector API Wrapper
=======================
This library is used to easily consume [DataCollector API](https://bitbucket.org/prodevcondev/data-collector-app/src/master/docs/api/api.md)
It takes an API response and maps it to appropriate models (objects) so it can be easily used in any application. Check models for available mathod calls.

Auth Token is required for API calls. Please make sure you accquire the token before using the library.

```php
use DataCollector\Api\Client;
use DataCollector\Api\Service\Url\UrlApiService;
use DataCollector\Api\Service\Company\CompanyApiService;
use DataCollector\Api\Service\Company\CompanyImportApiService;

/* Auhtenticate User */
$authToken = '###auth_token###';
$client = new Client($authToken);
```

## Available Services:
##### Company Service
```php
/* Initialize service */
$companyApiService = new CompanyApiService($client);

/* Get Company by id */
$request = $companyApiService->get(5347, CompanyApiService::RESPONSE_JSON);

/* Get Company by url string */
$request = $companyApiService->getByUrl('http://www.felbo.ch', CompanyApiService::RESPONSE_JSON);

/* Find Companies by keyword */
$request = $companyApiService->find('Computer Repair', CompanyApiService::RESPONSE_JSON);

/* Get Companies by import name */
$request = $companyApiService->getByImportName([
	'e-fon ag',
	'1 Computer',
	'abraxas informatik ag'
], CompanyApiService::RESPONSE_JSON);

/** Get Companies by import */
$request = $companyApiService->getByImport([
		'name'      => 'e-fon ag',
		'country'   => 'switzerland',
		'in_queue'  => 1
	], 
	CompanyApiService::RESPONSE_JSON);

```

##### Company Import Service
```php
/* Initialize service */
$companyImportApiService = new CompanyImportApiService($client);

/* Get Company Import by rule id */
$request = $companyImportApiService->getByRule(54, CompanyImportApiService::RESPONSE_JSON);

/* Get Company Import by rule id with Google Analytics */
$request = $companyImportApiService->getByRuleWithGoogleAnalytics(54, CompanyImportApiService::RESPONSE_JSON);
```

##### URL Service
```php
/* Initialize service */
$urlApiService = new UrlApiService($client);

/* Get URL by id */
$request = $urlApiService->get(3011, UrlApiService::RESPONSE_JSON);

/* Get Url by url string */
$request = $urlApiService->getByUrl('http://www.felbo.ch', UrlApiService::RESPONSE_JSON);

/* Find Urls by keyword */
$request = $urlApiService->find('Computer Repair', UrlApiService::RESPONSE_JSON);

```

##### Kompass Service
```php
/* Initialize service */
$kompassApiService = new KompassApiService($client);

/* Get Kompass by import */
$request = $kompassApiService->getByImport([
		'name'      => 'e-fon ag',
		'country'   => 'switzerland'
	], KompassApiService::RESPONSE_JSON);

```

## Request
$request will have a \GuzzleHttp\Psr7\Psr7\Request object
```php
/* Get response headers */
$request->getHeaders() /* Array of response header values*/

/* Get response content */
$request->getBody();
```
## Result builder (ex. UrlApiService)
```php
/* Build Result / Serialize API Response to objects/models */

/* Url Service - Get */
$urlApiService->buildResult($request->getBody(), UrlApiService::URL, CompanyApiService::RESPONSE_JSON);
/* Url Service - Get By URL  */
$urlApiService->buildResult($request->getBody(), UrlApiService::URL_BY_URL, CompanyApiService::RESPONSE_JSON);
/* Url Service - Find */
$urlApiService->buildResult($request->getBody(), UrlApiService::URL_FIND, CompanyApiService::RESPONSE_JSON);

```

## Installing 

The recommended way to install DataCollector API Wrapper is through [Composer](http://getcomposer.org).
```bash
php composer.phar require prodevcon/dc-api-wrapper
```

After installing, you need to require Composer's autoloader and register annotation to AnnotationRegistry (required by [jms/serializer](https://github.com/schmittjoh/serializer)):

```php
use Doctrine\Common\Annotations\AnnotationRegistry;

$loader = require __DIR__ . '/vendor/autoload.php';
AnnotationRegistry::registerLoader([$loader, 'loadClass']);
```