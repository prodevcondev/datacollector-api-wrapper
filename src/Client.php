<?php 

namespace DataCollector\Api;

use Psr\Http\Message\ResponseInterface;

/**
 * Client class
 * 
 * @author Marko Curcic <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class Client
{
	const API_URL = 'https://dc.prolead.ch/api/v1/';

	/**
	 * Client
	 * 
	 * @var \GuzzleHttp\Client
	 */
	protected $client;

	/**
	 * Auth Token
	 * 
	 * @var string
	 */
	protected $authToken;

	/**
	 * Constructor of class
	 * 
	 * @param string $authToken Authoriztion token from DataCollector
	 */
	public function __construct($authToken)
	{
		$this->client = new \GuzzleHttp\Client([
			'base_uri' => Client::API_URL
		]);

		$this->authToken = $authToken;
	}

	/**
	 * Makes an HTTP request to the api target
	 * 
	 * @param string 			  $method  GET
	 * @param string 			  $uri     URI part of API Endpoint
	 * @param string 			  $format
	 * @param array<string,mixed> $options 
	 * 
	 * @return ResponseInterface
	 */
	public function request($method, $uri = '', $format='json', array $options = [])
	{
		$options = array_merge($options, [
			'headers' => [
				'X-AUTH-TOKEN' 	=> $this->authToken,
	            'Accept' 	 	=> $this->getAcceptHeader($format)
			]
		]);
		
		return $this->client->request($method, $uri, $options);
	}

	/**
	 * Return Accept Header string
	 * 
	 * @param string $format
	 * 
	 * @return string
	 */
	protected function getAcceptHeader($format)
	{
		switch ($format) {
			case 'xml':				
				return 'application/xhtml+xml,application/xml';			
			default:
				return 'text/json,application/json';
		}
	}
}