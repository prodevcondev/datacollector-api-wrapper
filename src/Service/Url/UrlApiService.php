<?php 

namespace DataCollector\Api\Service\Url;

use DataCollector\Api\Client;
use Psr\Http\Message\ResponseInterface;
use DataCollector\Api\Service\Result\UrlResult;
use DataCollector\Api\Service\AbstractApiService;
use DataCollector\Api\Service\Exception\InvalidArgumentException;

/**
 * Url API Service
 * 
 * @author Marko Curcic   <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class UrlApiService extends AbstractApiService
{
	/**
	 * @var string
	 */
	const URL = 'url/get/:id';

	/**
	 * @var string
	 */
	const URL_BY_URL = 'url/get/:url';

	/**
	 * @var string
	 */
	const URL_FIND = 'url/find/:keyword';

	/**
	 * Constructor of class
	 * 
	 * @param Client $client
	 */
	public function __construct(Client $client)
	{
		parent::__construct($client, UrlResult::class);
	}

	/**
	 * Get URL by id
	 * 
	 * @param  int $id Rule ID
	 * @param  string $format Response format
	 * 
	 * @return ResponseInterface
	 */
	public function get($id, $format = parent::RESPONSE_JSON)
	{
		if(!is_integer($id)) throw new InvalidArgumentException("\$id need to be integer, ".gettype($id)." given. ".json_encode($id));
		
		$endpoint = str_replace(':id', (string) $id, self::URL);
		$response = $this->request('GET', $endpoint, $format);

		return $response;
	}

	/**
	 * Get Url by url
	 * 
	 * @param  string $url
	 * @param  string $format Response format
	 * 
	 * @return ResponseInterface
	 */
	public function getByUrl($url, $format = parent::RESPONSE_JSON)
	{
		if(!is_string($url)) throw new InvalidArgumentException("\$url need to be string, ".gettype($url)." given. ".json_encode($url));

		$endpoint = str_replace(':url', $url, self::URL_BY_URL);
		$response = $this->request('GET', $endpoint, $format);

		return $response;
	}

	/**
	 * Find URL by keyword
	 * 
	 * @param  string $keyword 
	 * @param  string $format Response format
	 * 
	 * @return ResponseInterface
	 */
	public function find($keyword, $format = parent::RESPONSE_JSON)
	{
		if(!is_string($keyword)) throw new InvalidArgumentException("\$keyword need to be string, ".gettype($keyword)." given. ".json_encode($keyword));

		$endpoint = str_replace(':keyword', $keyword, self::URL_FIND);
		$response = $this->request('GET', $endpoint, $format);

		return $response;
	}
}