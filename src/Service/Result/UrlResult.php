<?php 

namespace DataCollector\Api\Service\Result;

use DataCollector\Api\Service\Url\UrlApiService;
use DataCollector\Api\Model\Url;

/**
 * Url Result
 * 
 * @author Marko Curcic   <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class UrlResult extends AbstractResult
{
	/**
	 * {@inheritDoc}
	 */
	public static function getMapperClass($endpoint)
	{
		$modelDef = [
			UrlApiService::URL => Url::class,
			UrlApiService::URL_BY_URL => Url::class,
			UrlApiService::URL_FIND => 'array<'.Url::class.'>'
		];

		return $modelDef[$endpoint];
	}
}