<?php 

namespace DataCollector\Api\Service\Result;

use JMS\Serializer\SerializerBuilder;

/**
 * Abstract Result class
 * 
 * @author Marko Curcic   <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
abstract class AbstractResult
{
	/**
	 * Build Result
	 *
	 * @param string $data
	 * @param string $endpoint
	 * @param string $format
	 *
	 * @return mixed
	 */
	public static function build($data, $endpoint, $format)
	{
		$serializer = SerializerBuilder::create()->build();
		$object = $serializer->deserialize($data, static::getMapperClass($endpoint), $format);

		return $object;
	}

	/**
	 * Get Mapper Class
	 *
	 * @param string $endpoint
	 *
	 * @return string
	 */
	abstract protected static function getMapperClass($endpoint);
}