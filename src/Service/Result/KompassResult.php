<?php 

namespace DataCollector\Api\Service\Result;

use DataCollector\Api\Model\Kompass;
use DataCollector\Api\Service\Company\KompassApiService;

/**
 * Kompass Result
 * 
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class KompassResult extends AbstractResult
{
    /**
     * {@inheritDoc}
     */
	public static function getMapperClass($endpoint)
	{
		$modelDef = [
			KompassApiService::ENDPOINT_BY_IMPORT => Kompass::class,
		];

		return $modelDef[$endpoint];
	}
}