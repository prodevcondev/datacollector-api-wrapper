<?php 

namespace DataCollector\Api\Service\Result;

use DataCollector\Api\Service\Company\CompanyApiService;
use DataCollector\Api\Model\Company;

/**
 * Company Result class
 * 
 * @author Marko Curcic   <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class CompanyResult extends AbstractResult
{
	/**
	 * {@inheritDoc}
	 */
	public static function getMapperClass($endpoint)
	{
		$modelDef = [
			CompanyApiService::COMPANY 				  => Company::class,
			CompanyApiService::COMPANY_BY_IMPORT	  => Company::class,
			CompanyApiService::COMPANY_BY_IMPORT_NAME => 'array<'.Company::class.'>',
			CompanyApiService::COMPANY_BY_URL 		  => 'array<'.Company::class.'>',
			CompanyApiService::COMPANY_FIND 		  => 'array<'.Company::class.'>'
		];

		return $modelDef[$endpoint];
	}
}