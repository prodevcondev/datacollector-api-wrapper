<?php 

namespace DataCollector\Api\Service\Result;

use DataCollector\Api\Service\Company\CompanyImportApiService;
use DataCollector\Api\Model\CompanyImport;
use DataCollector\Api\Model\GoogleAnalytics;

/**
 * Company Import Result class
 * 
 * @author Marko Curcic   <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class CompanyImportResult extends AbstractResult
{
	/**
	 * {@inheritDoc}
	 */
	public static function getMapperClass($endpoint)
	{
		$modelDef = [
			CompanyImportApiService::COMPANY_IMPORT_BY_RULE 					   => 'array<'.CompanyImport::class.'>',
			CompanyImportApiService::COMPANY_IMPORT_BY_RULE_WITH_GOOGLE_ANALYTICS  => 'array<'.GoogleAnalytics::class.'>',
		];

		return $modelDef[$endpoint];
	}
}