<?php 

namespace DataCollector\Api\Service\Company;

use DataCollector\Api\Client;
use Psr\Http\Message\ResponseInterface;
use DataCollector\Api\Service\AbstractApiService;
use DataCollector\Api\Service\Result\CompanyResult;
use DataCollector\Api\Service\Exception\InvalidArgumentException;

/**
 * Company Api Service
 * 
 * @author Marko Curcic   <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class CompanyApiService extends AbstractApiService
{
	const COMPANY 				 = 'company/get/:id';
	const COMPANY_FIND 			 = 'company/find/:keyword';
	const COMPANY_BY_URL 		 = 'company/get/by_url/:url';
	const COMPANY_BY_IMPORT 	 = 'company/get/by_import';
	const COMPANY_BY_IMPORT_NAME = 'company/get/by_import_name/:names';

	/**
	 * Constructor of class
	 * 
	 * @param Client $client
	 */
	public function __construct(Client $client)
	{
		parent::__construct($client, CompanyResult::class);
	}
	
	/**
	 * Get Company data by id
	 * 
	 * @param  integer $id Company ID
	 * @param  string $format Input format JSON/XML
	 * 
	 * @return ResponseInterface
	 */
	public function get($id, $format = parent::RESPONSE_JSON)
	{
		if (!is_integer($id)) {
			throw new InvalidArgumentException("\$id need to be integer, ".gettype($id)." given. ".json_encode($id));
		}
		
		$endpoint = str_replace(':id', (string) $id, self::COMPANY);
		$response = $this->request('GET', $endpoint, $format);

		return $response;
	}

	/**
	 * Get Company data By URL
	 * 
	 * @param  string $url URL
	 * @param  string $format Input format JSON/XML
	 * 
	 * @return ResponseInterface
	 */
	public function getByUrl($url, $format = parent::RESPONSE_JSON)
	{
		if (!is_string($url)) {
			throw new InvalidArgumentException("\$url need to be string, ".gettype($url)." given. ".json_encode($url));
		}

		$endpoint = str_replace(':url', $url, self::COMPANY_BY_URL);
		$response = $this->request('GET', $endpoint, $format);

		return $response;
	}

	/**
	 * Find Company data by keyword
	 * 
	 * @param  string $keyword Part of company name
	 * @param  string $format Input format JSON/XML
	 * 
	 * @return ResponseInterface
	 */
	public function find($keyword, $format = parent::RESPONSE_JSON)
	{
		if (!is_string($keyword)) {
			throw new InvalidArgumentException("\$keyword need to be string, ".gettype($keyword)." given. ".json_encode($keyword));
		}

		$endpoint = str_replace(':keyword', $keyword, self::COMPANY_FIND);
		$response = $this->request('GET', $endpoint, $format);

		return $response;
	}

	/**
	 * Find Company data by company import name
	 * 
	 * @param  mixed $names Company import name or Array of names
	 * @param  string $format Input format JSON/XML
	 * 
	 * @return ResponseInterface
	 */
	public function getByImportName($names, $format = parent::RESPONSE_JSON)
	{
		if (!is_string($names) && !is_array($names)) {
			throw new InvalidArgumentException("\$names need to be array or string, ".gettype($names)." given. ".json_encode($names));
		}

		if (is_array($names)) {
			$names = join(",", $names);
		}

		$endpoint = str_replace(':names', $names, self::COMPANY_BY_IMPORT_NAME);
		$response = $this->request('GET', $endpoint, $format);

		return $response;
	}

	/**
	 * Get Company data by company import
	 * 
	 * @param array<string,mixed> $query  Endpoint query parameters
	 * @param string 			  $format Input format JSON/XML
	 * 
	 * @return ResponseInterface
	 */
	public function getByImport($query, $format = parent::RESPONSE_JSON)
	{
		if (empty($query) || !key_exists('name', $query)) {
			throw new InvalidArgumentException("\Name and country need to be defined in query array, given. ".json_encode($query));
		}

		$response = $this->request('GET', $this->httpBuildQuery(self::COMPANY_BY_IMPORT, $query), $format);

		return $response;
	}
}