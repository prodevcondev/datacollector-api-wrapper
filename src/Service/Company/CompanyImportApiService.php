<?php 

namespace DataCollector\Api\Service\Company;

use DataCollector\Api\Client;
use Psr\Http\Message\ResponseInterface;
use DataCollector\Api\Service\AbstractApiService;
use DataCollector\Api\Service\Result\CompanyImportResult;
use DataCollector\Api\Service\Exception\InvalidArgumentException;

/**
 * Company Import API Service class
 * 
 * @author Marko Curcic   <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class CompanyImportApiService extends AbstractApiService
{
	/**
	 * @var string
	 */
	const COMPANY_IMPORT_BY_RULE = 'company_import/by_rule/get/:id';

	/**
	 * @var string
	 */
	const COMPANY_IMPORT_BY_RULE_WITH_GOOGLE_ANALYTICS = 'company_import/google_analytics/by_rule/get/:id';

	/**
	 * Constructor of class
	 * 
	 * @param Client $client
	 */
	public function __construct(Client $client)
	{
		parent::__construct($client, CompanyImportResult::class);
	}

	/**
	 * Get Company Import by rule id
	 * 
	 * @param  int $id Rule ID
	 * @param  string $format Response format
	 * 
	 * @throws InvalidArgumentException
	 * 
	 * @return ResponseInterface
	 */
	public function getByRule($id, $format = parent::RESPONSE_JSON)
	{
		if (! is_integer($id)) {
			throw new InvalidArgumentException("\$id need to be string, ".gettype($id)." given. ".json_encode($id));
		}

		$endpoint = str_replace(':id', (string) $id, self::COMPANY_IMPORT_BY_RULE);
		$response = $this->request('GET', $endpoint, $format);

		return $response;
	}

	/**
	 * Get Company Import by rule id with Google Analytics
	 * 
	 * @param  int $id Rule ID
	 * @param  string $format Response format
	 * 
	 * @throws InvalidArgumentException
	 * 
	 * @return ResponseInterface
	 */
	public function getByRuleWithGoogleAnalytics($id, $format = parent::RESPONSE_JSON)
	{
		if (! is_integer($id)) {
			throw new InvalidArgumentException("\$id need to be string, ".gettype($id)." given. ".json_encode($id));
		}

		$endpoint = str_replace(':id', (string) $id, self::COMPANY_IMPORT_BY_RULE_WITH_GOOGLE_ANALYTICS);
		$response = $this->request('GET', $endpoint, $format);

		return $response;	
	}
}