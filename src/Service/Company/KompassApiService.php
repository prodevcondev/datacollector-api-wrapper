<?php 

namespace DataCollector\Api\Service\Company;

use DataCollector\Api\Client;
use Psr\Http\Message\ResponseInterface;
use DataCollector\Api\Service\AbstractApiService;
use DataCollector\Api\Service\Result\KompassResult;
use DataCollector\Api\Service\Exception\InvalidArgumentException;
/**
 * Kompass Api Service class
 * 
 * @author Marko Curcic   <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class KompassApiService extends AbstractApiService
{
    const ENDPOINT_BY_IMPORT = 'kompass/get/by_import';

    /**
     * Constructor of class
     * 
     * @param Client $client
     */
    public function __construct(Client $client)
	{
		parent::__construct($client, KompassResult::class);
    }
    
    /**
	 * Get Kompass data by company import
	 * 
	 * @param  array<string,mixed>  $query  Endpoint query parameters
	 * @param  string 				$format Input format JSON/XML
	 * 
	 * @return ResponseInterface
	 */
	public function getByImport($query, $format = parent::RESPONSE_JSON)
	{
		if (empty($query) || !key_exists('name', $query)) {
			throw new InvalidArgumentException("\Name and country need to be defined in query array, given. ".json_encode($query));
		}

		$response = $this->request('GET', $this->httpBuildQuery(KompassApiService::ENDPOINT_BY_IMPORT, $query), $format);

		return $response;
	}
}