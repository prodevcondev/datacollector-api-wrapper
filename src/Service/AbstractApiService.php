<?php 

namespace DataCollector\Api\Service;

use DataCollector\Api\Client;
use Psr\Http\Message\ResponseInterface;
use DataCollector\Api\Service\Exception\ClientException;

/**
 * Abstract API Service
 * 
 * @author Marko Curcic   <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
abstract class AbstractApiService
{
	const RESPONSE_JSON = 'json';
	const RESPONSE_XML  = 'xml';

	/**
	 * Client
	 *
	 * @var Client
	 */
	protected $client;

	/**
	 * Result Builder Class
	 *
	 * @var class-string
	 */
	protected $resultBuilderClass;

	/**
	 * Undocumented function
	 *
	 * @param Client 	   $client
	 * @param class-string $resultBuilderClass
	 */
	public function __construct(Client $client, $resultBuilderClass)
	{
		$this->client = $client;
		$this->resultBuilderClass = $resultBuilderClass;
	}

	/**
	 * Request
	 *
	 * @param string $httpMethod
	 * @param string $endpoint
	 * @param string $format
	 *
	 * @throws ClientException
	 * 
	 * @return ResponseInterface
	 */
	protected function request($httpMethod, $endpoint, $format="json")
	{
		try {
			return $this->client->request($httpMethod, $endpoint, $format);
		}
		catch( \GuzzleHttp\Exception\ClientException $e ){
			throw new ClientException($e->getMessage(), $e->getRequest(), $e->getResponse());
		}
	}

	/**
	 * Build Result
	 *
	 * @param string $data
	 * @param string $endpoint
	 * @param string $format
	 *
	 * @return mixed
	 */
	public function buildResult($data, $endpoint, $format="json")
	{
		return $this->resultBuilderClass::build($data, $endpoint, $format);
	}

	/**
	 * Build Endpoint Http Query
	 * 
	 * @param string $endpoint
	 * @param array<string,string> $query
	 * 
	 * @return string
	 */
	public function httpBuildQuery($endpoint, $query)
	{
		return $endpoint .'?'.http_build_query($query);
	}
}