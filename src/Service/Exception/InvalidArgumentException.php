<?php 

namespace DataCollector\Api\Service\Exception;

/**
 * Invalid Argument Exception class
 * 
 * @author Marko Curcic   <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class InvalidArgumentException extends \Exception
{
}