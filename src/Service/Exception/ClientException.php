<?php 

namespace DataCollector\Api\Service\Exception;

/**
 * Client Exception class
 * 
 * @author Marko Curcic   <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class ClientException extends \GuzzleHttp\Exception\ClientException
{
    
}