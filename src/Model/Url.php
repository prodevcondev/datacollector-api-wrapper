<?php 

namespace DataCollector\Api\Model;

use DateTime;
use JMS\Serializer\Annotation as JMS;

/**
 * Url class
 * 
 * @author Marko Curcic <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class Url
{
	/**
     * ID
     * 
     * @JMS\Type("integer")
     * 
     * @var int
	 */
	protected $id;

	/**
     * Url
     * 
     * @JMS\Type("string")
     * 
     * @var string
	 */
	protected $url;

	/**
     * Hostname
     * 
     * @JMS\Type("string")
     * 
     * @var string
	*/
	protected $hostname;

    /**
     * Domain
     * 
     * @JMS\Type("string")
     * 
     * @var string
     */
    protected $domain;

	/**
     * Title
     * 
     * @JMS\Type("string")
     * 
     * @var string
     */
	protected $title;

	/**
     * Description
     * 
     * @JMS\Type("string")
     * 
     * @var string
     */
	protected $description;

	/**
     * Http Status
     * 
     * @JMS\Type("string")
     * 
     * @var int
     */
	protected $httpStatus;

    /**
     * Online Status
     * 
     * @JMS\Type("integer")
     * 
     * @var int
     */
    protected $onlineStatus;

	/**
     * Upddated At
     * 
     * @JMS\Type("DateTime")
     * 
     * @var DateTime
     */
    protected $updatedAt;

    /**
     * Website Data
     * 
     * @var WebsiteData
     * 
     * @JMS\Type("DataCollector\Api\Model\WebsiteData")
     */
    protected $websiteData;

    /**
     * Website Analytics
     * 
     * @JMS\Type("DataCollector\Api\Model\WebsiteAnalytics")
     * 
     * @var WebsiteAnalytics
     */
    protected $websiteAnalytics;

    /**
     * Built With Data
     * 
     * @JMS\Type("DataCollector\Api\Model\BuiltWithData")
     * 
     * @var BuiltWithData
     */
    protected $builtWithData;

    /**
     * WhoIs
     * 
     * @JMS\Type("DataCollector\Api\Model\WhoIs")
     * 
     * @var WhoIs
     */
    protected $whoIs;

    /**
     * Company
     * 
     * @JMS\Type("DataCollector\Api\Model\Company")
     * @JMS\SerializedName("company_data")
     * 
     * @var Company
     */
    protected $company;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param int $id
     *
     * @return self

     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set hostname
     *
     * @param string $hostname
     *
     * @return self
     */
    public function setHostname($hostname)
    {
        $this->hostname = $hostname;

        return $this;
    }

    /**
     * Get hostname
     *
     * @return string
     */
    public function getHostname()
    {
        return $this->hostname;
    }

    /**
     * Set domain
     *
     * @param string $domain
     *
     * @return self
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set httpStatus
     *
     * @param integer $httpStatus
     *
     * @return self
     */
    public function setHttpStatus($httpStatus)
    {
        $this->httpStatus = $httpStatus;

        return $this;
    }

    /**
     * Get httpStatus
     *
     * @return integer
     */
    public function getHttpStatus()
    {
        return $this->httpStatus;
    }

    /**
     * Set onlineStatus
     *
     * @param int $onlineStatus
     *
     * @return self
     */
    public function setOnlineStatus($onlineStatus)
    {
        $this->onlineStatus = $onlineStatus;

        return $this;
    }

    /**
     * Get onlineStatus
     *
     * @return integer
     */
    public function getOnlineStatus()
    {
        return $this->onlineStatus;
    }

    /**
     * Set updatedAt
     *
     * @param DateTime $updatedAt
     *
     * @return self
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set whois.
     *
     * @param WhoIs $whoIs
     *
     * @return self
     */
    public function setWhoIs(WhoIs $whoIs)
    {
        $this->whoIs = $whoIs;

        return $this;
    }

    /**
     * Get whois.
     *
     * @return WhoIs
     */
    public function getWhoIs()
    {
        return $this->whoIs;
    }

    /**
     * Set company
     *
     * @param Company $company
     *
     * @return self
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company.
     *
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set websiteData
     *
     * @param WebsiteData $websiteData
     *
     * @return self
     */
    public function setWebsiteData(WebsiteData $websiteData)
    {
        $this->websiteData = $websiteData;

        return $this;
    }

    /**
     * Get websiteData
     *
     * @return WebsiteData
     */
    public function getWebsiteData()
    {
        return $this->websiteData;
    }

    /**
     * Set websiteAnalytics
     *
     * @param WebsiteAnalytics $websiteAnalytics
     *
     * @return self
     */
    public function setWebsiteAnalytics(WebsiteAnalytics $websiteAnalytics)
    {
        $this->websiteAnalytics = $websiteAnalytics;

        return $this;
    }

    /**
     * Get websiteAnalytics
     *
     * @return WebsiteAnalytics
     */
    public function getWebsiteAnalytics()
    {
        return $this->websiteAnalytics;
    }

    /**
     * Set builtWithData
     *
     * @param BuiltWithData $builtWithData
     *
     * @return self
     */
    public function setBuiltWithData(BuiltWithData $builtWithData)
    {
        $this->builtWithData = $builtWithData;

        return $this;
    }

    /**
     * Get builtWithData
     *
     * @return BuiltWithData
     */
    public function getBuiltWithData()
    {
        return $this->builtWithData;
    }
}