<?php

namespace DataCollector\Api\Model;

use JMS\Serializer\Annotation as JMS;
/**
 * Company Import class
 * 
 * @author Marko Curcic <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class CompanyImport
{
    /**
     * @var string
     *
     * @JMS\Type("string")
     */
    protected $name;

    /**
     * @var \DataCollector\Api\Model\Company
     *
     * @JMS\Type("DataCollector\Api\Model\Company")
     * @JMS\SerializedName("company_data")
     */
    protected $company;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CompanyImport
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set company
     *
     * @param \DataCollector\Api\Model\Company $company
     *
     * @return \DataCollector\Api\Model\CompanyImport
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company.
     *
     * @return \DataCollector\Api\Model\Company
     */
    public function getCompany()
    {
        return $this->company;
    }
}