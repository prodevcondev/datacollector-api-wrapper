<?php

namespace DataCollector\Api\Model;

use JMS\Serializer\Annotation as JMS;
use DataCollector\Api\Model\PaymentSystem;
/**
 * Built With Data class
 * 
 * @author Marko Curcic <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class BuiltWithData
{
    /**
     * @var int
     * 
     * @JMS\Type("integer")
     */
    protected $id;

    /**
     * @var boolean
     *
     * @JMS\Type("boolean")
     */
    protected $analyticsAndTracking;

    /**
     * @var string
     *
     * @JMS\Type("string")
     */
    protected $cms;

    /**
     * @var array<string>
     *
     * @JMS\Type("array<string>")
     */
    protected $hostingProviders;

    /**
     * @var array<string>
     *
     * @JMS\Type("array<string>")
     */
    protected $paymentProviders;

    /**
     * @var array<string>
     *
     * @JMS\Type("array<string>")
     */
    protected $sslCertificates;

    /**
     * @var string
     *
     * @JMS\Type("string")
     */
    protected $encoding;

    /**
     * @var string
     *
     * @JMS\Type("string")
     */
    protected $webServer;

    /**
     * @var boolean
     *
     * @JMS\Type("boolean")
     */
    protected $ecommerce;

    /**
     * @var \DateTime
     * 
     * @JMS\Type("DateTime")
     */
    protected $updatedAt;

    /**
     * @var PaymentSystem[]
     * 
     * @JMS\Type("array<DataCollector\Api\Model\PaymentSystem>") 
     */
    protected $paymentSystems;

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set analyticsAndTracking
     *
     * @param boolean $analyticsAndTracking
     *
     * @return self
     */
    public function setAnalyticsAndTracking($analyticsAndTracking)
    {
        $this->analyticsAndTracking = $analyticsAndTracking;

        return $this;
    }

    /**
     * Get analyticsAndTracking
     *
     * @return boolean
     */
    public function getAnalyticsAndTracking()
    {
        return $this->analyticsAndTracking;
    }

    /**
     * Set cms
     *
     * @param string $cms
     *
     * @return self
     */
    public function setCms($cms)
    {
        $this->cms = $cms;

        return $this;
    }

    /**
     * Get cms
     *
     * @return string
     */
    public function getCms()
    {
        return $this->cms;
    }

    /**
     * Set hostingProviders
     *
     * @param array<string> $hostingProviders
     *
     * @return self
     */
    public function setHostingProviders($hostingProviders)
    {
        $this->hostingProviders = $hostingProviders;

        return $this;
    }

    /**
     * Get hostingProviders
     *
     * @return array<string>
     */
    public function getHostingProviders()
    {
        return $this->hostingProviders;
    }

    /**
     * Set paymentProviders
     *
     * @param array<string> $paymentProviders
     *
     * @return self
     */
    public function setPaymentProviders($paymentProviders)
    {
        $this->paymentProviders = $paymentProviders;

        return $this;
    }

    /**
     * Get paymentProviders
     *
     * @return array<string>
     */
    public function getPaymentProviders()
    {
        return $this->paymentProviders;
    }

    /**
     * Set sslCertificates
     *
     * @param array<string> $sslCertificates
     *
     * @return self
     */
    public function setSslCertificates($sslCertificates)
    {
        $this->sslCertificates = $sslCertificates;

        return $this;
    }

    /**
     * Get sslCertificates
     *
     * @return array<string>
     */
    public function getSslCertificates()
    {
        return $this->sslCertificates;
    }

    /**
     * Set encoding
     *
     * @param string $encoding
     *
     * @return self
     */
    public function setEncoding($encoding)
    {
        $this->encoding = $encoding;

        return $this;
    }

    /**
     * Get encoding
     *
     * @return string
     */
    public function getEncoding()
    {
        return $this->encoding;
    }

    /**
     * Set webServer
     *
     * @param string $webServer
     *
     * @return self
     */
    public function setWebServer($webServer)
    {
        $this->webServer = $webServer;

        return $this;
    }

    /**
     * Get webServer
     *
     * @return string
     */
    public function getWebServer()
    {
        return $this->webServer;
    }

    /**
     * Set ecommerce
     *
     * @param boolean $ecommerce
     *
     * @return self
     */
    public function setEcommerce($ecommerce)
    {
        $this->ecommerce = $ecommerce;

        return $this;
    }

    /**
     * Get ecommerce
     *
     * @return boolean
     */
    public function getEcommerce()
    {
        return $this->ecommerce;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return self
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set paymentSystems
     *
     * @param PaymentSystem[] $paymentSystems
     *
     * @return self
     */
    public function setPaymentSystems($paymentSystems)
    {
        $this->paymentSystems = $paymentSystems;

        return $this;
    }

    /**
     * Get paymentSystems
     *
     * @return PaymentSystem[]
     */
    public function getPaymentSystems()
    {
        return $this->paymentSystems;
    }
}
