<?php 

namespace DataCollector\Api\Model;

use DataCollector\Api\Model\Url;
use JMS\Serializer\Annotation as JMS;

/**
 * Company class
 * 
 * @author Marko Curcic <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class Company
{
    /**
     * @var int
     * 
     * @JMS\Type("integer")
     */
    protected $id;

    /**
     * @var int
     *
     * @JMS\Type("integer")
     */
    protected $score;

    /**
     * @var string
     *
     * @JMS\Type("string")
     */
    protected $contactName;

    /**
     * @var \DateTime
     *
     * @JMS\Type("DateTime")
     */
    protected $updatedAt;

    /**
     * Urls
     * 
     * @var Url[]
     *
     * @JMS\Type("array<DataCollector\Api\Model\Url>")
     */
    protected $urls;

    /**
     * @var \DataCollector\Api\Model\GooglePlace
     * 
     * @JMS\Type("DataCollector\Api\Model\GooglePlace")
     */
    protected $googlePlace;

    /**
     * @var \DataCollector\Api\Model\Zefix
     *
     * @JMS\Type("DataCollector\Api\Model\Zefix")
     */
    protected $zefix;

    /**
     * @var \DataCollector\Api\Model\TelSearch
     *
     * @JMS\Type("DataCollector\Api\Model\TelSearch")
     */
    protected $telSearch;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Company

     */
    public function setId($id)
    {
    	$this->id = $id;

    	return $this;
    }

    /**
     * Set score
     *
     * @param integer $score
     *
     * @return Company

     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return int
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set contactName
     *
     * @param string $contactName
     *
     * @return Company

     */
    public function setContactName($contactName)
    {
        $this->contactName = $contactName;

        return $this;
    }

    /**
     * Get contactName
     *
     * @return string
     */
    public function getContactName()
    {
        return $this->contactName;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Company

     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set urls.
     *
     * @param Url[] $urls
     * 
     * @return Company
     */
    public function setUrls($urls)
    {
        $this->urls = $urls;

        return $this;
    }

    /**
     * Get urls.
     *
     * @return Url[]
     */
    public function getUrls()
    {
        return $this->urls;
    }

    /**
     * Set googlePlace
     *
     * @param \DataCollector\Api\Model\GooglePlace $googlePlace
     *
     * @return Company
     */
    public function setGooglePlace(GooglePlace $googlePlace)
    {
        $this->googlePlace = $googlePlace;

        return $this;
    }

    /**
     * Get googlePlace
     *
     * @return \DataCollector\Api\Model\GooglePlace
     */
    public function getGooglePlace()
    {
        return $this->googlePlace;
    }

    /**
     * Set zefix.
     *
     * @param \DataCollector\Api\Model\Zefix $zefix
     * 
     * @return Company
     */
    public function setZefix(Zefix $zefix)
    {
        $this->zefix = $zefix;

        return $this;
    }

    /**
     * Get zefix.
     *
     * @return \DataCollector\Api\Model\Zefix
     */
    public function getZefix()
    {
        return $this->zefix;
    }

    /**
     * Set telSearch.
     *
     * @param \DataCollector\Api\Model\TelSearch $telSearch
     * 
     * @return Company
     */
    public function setTelSearch(TelSearch $telSearch)
    {
        $this->telSearch = $telSearch;

        return $this;
    }

    /**
     * Get telSearch.
     *
     * @return \DataCollector\Api\Model\TelSearch
     */
    public function getTelSearch()
    {
        return $this->telSearch;
    }
}