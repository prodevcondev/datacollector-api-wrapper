<?php

namespace DataCollector\Api\Model;

use DateTime;
use JMS\Serializer\Annotation as JMS;

/**
 * Website Analytics class
 * 
 * @author Marko Curcic <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class WebsiteAnalytics
{
    /**
     * ID
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $id;

    /**
     * Score
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $score;

    /**
     * Moz Subdomain Normalized
     * 
     * @JMS\Type("float")
     *
     * @var float
     */
    protected $mozSubdomainNormalized;

    /**
     * Moz Subdomain Raw
     * 
     * @JMS\Type("float")
     *
     * @var float
     */
    protected $mozSubdomainRaw;

    /**
     * Moz Url Normalized
     * 
     * @JMS\Type("float")
     *
     * @var float
     */
    protected $mozUrlNormalized;

    /**
     * Moz Url Raw
     * 
     * @JMS\Type("float")
     *
     * @var float
     */
    protected $mozUrlRaw;

    /**
     * Moz Domain Authority
     * 
     * @JMS\Type("float")
     *
     * @var float
     */
    protected $mozDomainAuthority;

    /**
     * Moz Page Authority
     * 
     * @JMS\Type("float")
     *
     * @var float
     */
    protected $mozPageAuthority;

    /**
     * Moz External Equity Links
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $mozExternalEquityLinks;

    /**
     * Moz Links
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $mozLinks;

    /**
     * Moz Rank
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $mozRank;

    /**
     * ISP Name
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $ispName;

    /**
     * ISP ID
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $ispIp;

    /**
     * ISP City
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $ispCity;

    /**
     * ISP Region
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $ispRegion;

    /**
     * ISP Country
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $ispCountry;

    /**
     * ISP Time Zone
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $ispTimeZone;

    /**
     * ISP Longitude
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $ispLongitude;

    /**
     * ISP Latitude
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $ispLatitude;

    /**
     * Malware Status Google
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $malwareStatusGoogle;

    /**
     * Malware Status Mcafee
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $malwareStatusMcafee;

    /**
     * Malware Status Avg
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $malwareStatusAvg;

    /**
     * Malware Status Norton
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $malwareStatusNorton;

    /**
     * Search Index Google
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $searchIndexGoogle;

    /**
     * Search Index Bing
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $searchIndexBing;

    /**
     * Search Index Yahoo
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $searchIndexYahoo;

    /**
     * Is Mobile Ready
     * 
     * @JMS\Type("boolean")
     *
     * @var bool
     */
    protected $isMobileReady;

    /**
     * Page Speed Mobile
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $pagespeedMobile;

    /**
     * Page Speed Desktop
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $pagespeedDesktop;

    /**
     * Social Facebook
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $socialFacebook;

    /**
     * Social Google Plus
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $socialGooglePlus;

    /**
     * Social LinkedIn
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $socialLinkedIn;

    /**
     * Social Pinterest
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $socialPinterest;

    /**
     * Social Xing
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $socialXing;

    /**
     * Is IP Blacklisted
     * 
     * @JMS\Type("boolean")
     *
     * @var bool
     */
    protected $isIpBlacklisted;

    /**
     * Alexa Global Rank
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $alexaGlobalRank;

    /**
     * Alexa Country Rank
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $alexaCountryRank;

    /**
     * W3C Errors
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $w3cErrors;

    /**
     * Has Google Analitycs
     * 
     * @JMS\Type("boolean")
     *
     * @var bool
     */
    protected $hasGoogleAnalitycs;

    /**
     * CMS
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $cms;

    /**
     * Server Name
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $serverName;

    /**
     * Server OS
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $serverOs;

    /**
     * Has SSL
     * 
     * @JMS\Type("boolean")
     *
     * @var bool
     */
    protected $hasSsl;

    /**
     * Has Archive
     * 
     * @JMS\Type("boolean")
     *
     * @var bool
     */
    protected $hasArchive;

    /**
     * Updated At
     * 
     * @JMS\Type("DateTime")
     *
     * @var DateTime
     */
    protected $updatedAt;

    /**
     * Set id
     *
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set score
     *
     * @param int $score
     *
     * @return self
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return int
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set mozSubdomainNormalized
     *
     * @param float $mozSubdomainNormalized
     *
     * @return self
     */
    public function setMozSubdomainNormalized($mozSubdomainNormalized)
    {
        $this->mozSubdomainNormalized = $mozSubdomainNormalized;

        return $this;
    }

    /**
     * Get mozSubdomainNormalized
     *
     * @return float
     */
    public function getMozSubdomainNormalized()
    {
        return $this->mozSubdomainNormalized;
    }

    /**
     * Set mozSubdomainRaw
     *
     * @param float $mozSubdomainRaw
     *
     * @return self
     */
    public function setMozSubdomainRaw($mozSubdomainRaw)
    {
        $this->mozSubdomainRaw = $mozSubdomainRaw;

        return $this;
    }

    /**
     * Get mozSubdomainRaw
     *
     * @return float
     */
    public function getMozSubdomainRaw()
    {
        return $this->mozSubdomainRaw;
    }

    /**
     * Set mozUrlNormalized
     *
     * @param float $mozUrlNormalized
     *
     * @return self
     */
    public function setMozUrlNormalized($mozUrlNormalized)
    {
        $this->mozUrlNormalized = $mozUrlNormalized;

        return $this;
    }

    /**
     * Get mozUrlNormalized
     *
     * @return float
     */
    public function getMozUrlNormalized()
    {
        return $this->mozUrlNormalized;
    }

    /**
     * Set mozUrlRaw
     *
     * @param float $mozUrlRaw
     *
     * @return self
     */
    public function setMozUrlRaw($mozUrlRaw)
    {
        $this->mozUrlRaw = $mozUrlRaw;

        return $this;
    }

    /**
     * Get mozUrlRaw
     *
     * @return float
     */
    public function getMozUrlRaw()
    {
        return $this->mozUrlRaw;
    }

    /**
     * Set mozDomainAuthority
     *
     * @param float $mozDomainAuthority
     *
     * @return self
     */
    public function setMozDomainAuthority($mozDomainAuthority)
    {
        $this->mozDomainAuthority = $mozDomainAuthority;

        return $this;
    }

    /**
     * Get mozDomainAuthority
     *
     * @return float
     */
    public function getMozDomainAuthority()
    {
        return $this->mozDomainAuthority;
    }

    /**
     * Set mozPageAuthority
     *
     * @param float $mozPageAuthority
     *
     * @return self
     */
    public function setMozPageAuthority($mozPageAuthority)
    {
        $this->mozPageAuthority = $mozPageAuthority;

        return $this;
    }

    /**
     * Get mozPageAuthority
     *
     * @return float
     */
    public function getMozPageAuthority()
    {
        return $this->mozPageAuthority;
    }

    /**
     * Set mozExternalEquityLinks
     *
     * @param int $mozExternalEquityLinks
     *
     * @return self
     */
    public function setMozExternalEquityLinks($mozExternalEquityLinks)
    {
        $this->mozExternalEquityLinks = $mozExternalEquityLinks;

        return $this;
    }

    /**
     * Get mozExternalEquityLinks
     *
     * @return int
     */
    public function getMozExternalEquityLinks()
    {
        return $this->mozExternalEquityLinks;
    }

    /**
     * Set mozLinks
     *
     * @param int $mozLinks
     *
     * @return self
     */
    public function setMozLinks($mozLinks)
    {
        $this->mozLinks = $mozLinks;

        return $this;
    }

    /**
     * Get mozLinks
     *
     * @return int
     */
    public function getMozLinks()
    {
        return $this->mozLinks;
    }

    /**
     * Set mozRank
     *
     * @param int $mozRank
     *
     * @return self
     */
    public function setMozRank($mozRank)
    {
        $this->mozRank = $mozRank;

        return $this;
    }

    /**
     * Get mozRank
     *
     * @return int
     */
    public function getMozRank()
    {
        return $this->mozRank;
    }

    /**
     * Set ispName
     *
     * @param string $ispName
     *
     * @return self
     */
    public function setIspName($ispName)
    {
        $this->ispName = $ispName;

        return $this;
    }

    /**
     * Get ispName
     *
     * @return string
     */
    public function getIspName()
    {
        return $this->ispName;
    }

    /**
     * Set ispIp
     *
     * @param string $ispIp
     *
     * @return self
     */
    public function setIspIp($ispIp)
    {
        $this->ispIp = $ispIp;

        return $this;
    }

    /**
     * Get ispIp
     *
     * @return string
     */
    public function getIspIp()
    {
        return $this->ispIp;
    }

    /**
     * Set ispCity
     *
     * @param string $ispCity
     *
     * @return self
     */
    public function setIspCity($ispCity)
    {
        $this->ispCity = $ispCity;

        return $this;
    }

    /**
     * Get ispCity
     *
     * @return string
     */
    public function getIspCity()
    {
        return $this->ispCity;
    }

    /**
     * Set ispRegion
     *
     * @param string $ispRegion
     *
     * @return self
     */
    public function setIspRegion($ispRegion)
    {
        $this->ispRegion = $ispRegion;

        return $this;
    }

    /**
     * Get ispRegion
     *
     * @return string
     */
    public function getIspRegion()
    {
        return $this->ispRegion;
    }

    /**
     * Set ispCountry
     *
     * @param string $ispCountry
     *
     * @return self
     */
    public function setIspCountry($ispCountry)
    {
        $this->ispCountry = $ispCountry;

        return $this;
    }

    /**
     * Get ispCountry
     *
     * @return string
     */
    public function getIspCountry()
    {
        return $this->ispCountry;
    }

    /**
     * Set ispTimeZone
     *
     * @param string $ispTimeZone
     *
     * @return self
     */
    public function setIspTimeZone($ispTimeZone)
    {
        $this->ispTimeZone = $ispTimeZone;

        return $this;
    }

    /**
     * Get ispTimeZone
     *
     * @return string
     */
    public function getIspTimeZone()
    {
        return $this->ispTimeZone;
    }

    /**
     * Set ispLongitude
     *
     * @param string $ispLongitude
     *
     * @return self
     */
    public function setIspLongitude($ispLongitude)
    {
        $this->ispLongitude = $ispLongitude;

        return $this;
    }

    /**
     * Get ispLongitude
     *
     * @return string
     */
    public function getIspLongitude()
    {
        return $this->ispLongitude;
    }

    /**
     * Set ispLatitude
     *
     * @param string $ispLatitude
     *
     * @return self
     */
    public function setIspLatitude($ispLatitude)
    {
        $this->ispLatitude = $ispLatitude;

        return $this;
    }

    /**
     * Get ispLatitude
     *
     * @return string
     */
    public function getIspLatitude()
    {
        return $this->ispLatitude;
    }

    /**
     * Set malwareStatusGoogle
     *
     * @param string $malwareStatusGoogle
     *
     * @return self
     */
    public function setMalwareStatusGoogle($malwareStatusGoogle)
    {
        $this->malwareStatusGoogle = $malwareStatusGoogle;

        return $this;
    }

    /**
     * Get malwareStatusGoogle
     *
     * @return string
     */
    public function getMalwareStatusGoogle()
    {
        return $this->malwareStatusGoogle;
    }

    /**
     * Set malwareStatusMcafee
     *
     * @param string $malwareStatusMcafee
     *
     * @return self
     */
    public function setMalwareStatusMcafee($malwareStatusMcafee)
    {
        $this->malwareStatusMcafee = $malwareStatusMcafee;

        return $this;
    }

    /**
     * Get malwareStatusMcafee
     *
     * @return string
     */
    public function getMalwareStatusMcafee()
    {
        return $this->malwareStatusMcafee;
    }

    /**
     * Set malwareStatusAvg
     *
     * @param string $malwareStatusAvg
     *
     * @return self
     */
    public function setMalwareStatusAvg($malwareStatusAvg)
    {
        $this->malwareStatusAvg = $malwareStatusAvg;

        return $this;
    }

    /**
     * Get malwareStatusAvg
     *
     * @return string
     */
    public function getMalwareStatusAvg()
    {
        return $this->malwareStatusAvg;
    }

    /**
     * Set malwareStatusNorton
     *
     * @param string $malwareStatusNorton
     *
     * @return self
     */
    public function setMalwareStatusNorton($malwareStatusNorton)
    {
        $this->malwareStatusNorton = $malwareStatusNorton;

        return $this;
    }

    /**
     * Get malwareStatusNorton
     *
     * @return string
     */
    public function getMalwareStatusNorton()
    {
        return $this->malwareStatusNorton;
    }

    /**
     * Set searchIndexGoogle
     *
     * @param int $searchIndexGoogle
     *
     * @return self
     */
    public function setSearchIndexGoogle($searchIndexGoogle)
    {
        $this->searchIndexGoogle = $searchIndexGoogle;

        return $this;
    }

    /**
     * Get searchIndexGoogle
     *
     * @return int
     */
    public function getSearchIndexGoogle()
    {
        return $this->searchIndexGoogle;
    }

    /**
     * Set searchIndexBing
     *
     * @param int $searchIndexBing
     *
     * @return self
     */
    public function setSearchIndexBing($searchIndexBing)
    {
        $this->searchIndexBing = $searchIndexBing;

        return $this;
    }

    /**
     * Get searchIndexBing
     *
     * @return int
     */
    public function getSearchIndexBing()
    {
        return $this->searchIndexBing;
    }

    /**
     * Set searchIndexYahoo
     *
     * @param int $searchIndexYahoo
     *
     * @return self
     */
    public function setSearchIndexYahoo($searchIndexYahoo)
    {
        $this->searchIndexYahoo = $searchIndexYahoo;

        return $this;
    }

    /**
     * Get searchIndexYahoo
     *
     * @return int
     */
    public function getSearchIndexYahoo()
    {
        return $this->searchIndexYahoo;
    }

    /**
     * Set isMobileReady
     *
     * @param boolean $isMobileReady
     *
     * @return self
     */
    public function setIsMobileReady($isMobileReady)
    {
        $this->isMobileReady = $isMobileReady;

        return $this;
    }

    /**
     * Get isMobileReady
     *
     * @return bool
     */
    public function getIsMobileReady()
    {
        return $this->isMobileReady;
    }

    /**
     * Set pagespeedMobile
     *
     * @param int $pagespeedMobile
     *
     * @return self
     */
    public function setPagespeedMobile($pagespeedMobile)
    {
        $this->pagespeedMobile = $pagespeedMobile;

        return $this;
    }

    /**
     * Get pagespeedMobile
     *
     * @return int
     */
    public function getPagespeedMobile()
    {
        return $this->pagespeedMobile;
    }

    /**
     * Set pagespeedDesktop
     *
     * @param int $pagespeedDesktop
     *
     * @return self
     */
    public function setPagespeedDesktop($pagespeedDesktop)
    {
        $this->pagespeedDesktop = $pagespeedDesktop;

        return $this;
    }

    /**
     * Get pagespeedDesktop
     *
     * @return int
     */
    public function getPagespeedDesktop()
    {
        return $this->pagespeedDesktop;
    }

    /**
     * Set socialFacebook
     *
     * @param int $socialFacebook
     *
     * @return self
     */
    public function setSocialFacebook($socialFacebook)
    {
        $this->socialFacebook = $socialFacebook;

        return $this;
    }

    /**
     * Get socialFacebook
     *
     * @return int
     */
    public function getSocialFacebook()
    {
        return $this->socialFacebook;
    }

    /**
     * Set socialGooglePlus
     *
     * @param int $socialGooglePlus
     *
     * @return self
     */
    public function setSocialGooglePlus($socialGooglePlus)
    {
        $this->socialGooglePlus = $socialGooglePlus;

        return $this;
    }

    /**
     * Get socialGooglePlus
     *
     * @return int
     */
    public function getSocialGooglePlus()
    {
        return $this->socialGooglePlus;
    }

    /**
     * Set socialLinkedIn
     *
     * @param int $socialLinkedIn
     *
     * @return self
     */
    public function setSocialLinkedIn($socialLinkedIn)
    {
        $this->socialLinkedIn = $socialLinkedIn;

        return $this;
    }

    /**
     * Get socialLinkedIn
     *
     * @return int
     */
    public function getSocialLinkedIn()
    {
        return $this->socialLinkedIn;
    }

    /**
     * Set socialPinterest
     *
     * @param int $socialPinterest
     *
     * @return self
     */
    public function setSocialPinterest($socialPinterest)
    {
        $this->socialPinterest = $socialPinterest;

        return $this;
    }

    /**
     * Get socialPinterest
     *
     * @return int
     */
    public function getSocialPinterest()
    {
        return $this->socialPinterest;
    }

    /**
     * Set socialXing
     *
     * @param int $socialXing
     *
     * @return self
     */
    public function setSocialXing($socialXing)
    {
        $this->socialXing = $socialXing;

        return $this;
    }

    /**
     * Get socialXing
     *
     * @return int
     */
    public function getSocialXing()
    {
        return $this->socialXing;
    }

    /**
     * Set isIpBlacklisted
     *
     * @param boolean $isIpBlacklisted
     *
     * @return self
     */
    public function setIsIpBlacklisted($isIpBlacklisted)
    {
        $this->isIpBlacklisted = $isIpBlacklisted;

        return $this;
    }

    /**
     * Get isIpBlacklisted
     *
     * @return bool
     */
    public function getIsIpBlacklisted()
    {
        return $this->isIpBlacklisted;
    }

    /**
     * Set alexaGlobalRank
     *
     * @param int $alexaGlobalRank
     *
     * @return self
     */
    public function setAlexaGlobalRank($alexaGlobalRank)
    {
        $this->alexaGlobalRank = $alexaGlobalRank;

        return $this;
    }

    /**
     * Get alexaGlobalRank
     *
     * @return int
     */
    public function getAlexaGlobalRank()
    {
        return $this->alexaGlobalRank;
    }

    /**
     * Set alexaCountryRank
     *
     * @param int $alexaCountryRank
     *
     * @return self
     */
    public function setAlexaCountryRank($alexaCountryRank)
    {
        $this->alexaCountryRank = $alexaCountryRank;

        return $this;
    }

    /**
     * Get alexaCountryRank
     *
     * @return int
     */
    public function getAlexaCountryRank()
    {
        return $this->alexaCountryRank;
    }

    /**
     * Set w3cErrors
     *
     * @param int $w3cErrors
     *
     * @return self
     */
    public function setW3cErrors($w3cErrors)
    {
        $this->w3cErrors = $w3cErrors;

        return $this;
    }

    /**
     * Get w3cErrors
     *
     * @return int
     */
    public function getW3cErrors()
    {
        return $this->w3cErrors;
    }

    /**
     * Set hasGoogleAnalitycs
     *
     * @param boolean $hasGoogleAnalitycs
     *
     * @return self
     */
    public function setHasGoogleAnalitycs($hasGoogleAnalitycs)
    {
        $this->hasGoogleAnalitycs = $hasGoogleAnalitycs;

        return $this;
    }

    /**
     * Get hasGoogleAnalitycs
     *
     * @return bool
     */
    public function getHasGoogleAnalitycs()
    {
        return $this->hasGoogleAnalitycs;
    }

    /**
     * Set cms
     *
     * @param string $cms
     *
     * @return self
     */
    public function setCms($cms)
    {
        $this->cms = $cms;

        return $this;
    }

    /**
     * Get cms
     *
     * @return string
     */
    public function getCms()
    {
        return $this->cms;
    }

    /**
     * Set serverName
     *
     * @param string $serverName
     *
     * @return self
     */
    public function setServerName($serverName)
    {
        $this->serverName = $serverName;

        return $this;
    }

    /**
     * Get serverName
     *
     * @return string
     */
    public function getServerName()
    {
        return $this->serverName;
    }

    /**
     * Set serverOs
     *
     * @param string $serverOs
     *
     * @return self
     */
    public function setServerOs($serverOs)
    {
        $this->serverOs = $serverOs;

        return $this;
    }

    /**
     * Get serverOs
     *
     * @return string
     */
    public function getServerOs()
    {
        return $this->serverOs;
    }

    /**
     * Set hasSsl
     *
     * @param boolean $hasSsl
     *
     * @return self
     */
    public function setHasSsl($hasSsl)
    {
        $this->hasSsl = $hasSsl;

        return $this;
    }

    /**
     * Get hasSsl
     *
     * @return bool
     */
    public function getHasSsl()
    {
        return $this->hasSsl;
    }

    /**
     * Set hasArchive
     *
     * @param boolean $hasArchive
     *
     * @return self
     */
    public function setHasArchive($hasArchive)
    {
        $this->hasArchive = $hasArchive;

        return $this;
    }

    /**
     * Get hasArchive
     *
     * @return bool
     */
    public function getHasArchive()
    {
        return $this->hasArchive;
    }

    /**
     * Set updatedAt
     *
     * @param DateTime $updatedAt
     *
     * @return self
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
