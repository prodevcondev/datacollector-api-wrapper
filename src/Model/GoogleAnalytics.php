<?php

namespace DataCollector\Api\Model;

use JMS\Serializer\Annotation as JMS;


/**
 * Google Analytics class
 * 
 * @author Marko Curcic <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class GoogleAnalytics extends CompanyImport
{
	/**
	 * @var GoogleAnalyticsSession
	 * 
	 * @JMS\Type("array<DataCollector\Api\Model\GoogleAnalyticsSession>")
	 */
	protected $googleAnalyticsSessions;

	/**
	 * Set Goolge Analytics Session
	 * 
	 * @param GoogleAnalyticsSession $googleAnalyticsSessions
	 *
	 * @return self
	 */
	public function setGoogleAnalyticsSessions($googleAnalyticsSessions) : self
	{
		$this->googleAnalyticsSessions = $googleAnalyticsSessions;

		return $this;
	}

	/**
	 * Get Goolge Analytics Session
	 * 
	 * @return GoogleAnalyticsSession
	 */
	public function getGoogleAnalyticsSessions() : GoogleAnalyticsSession
	{
		return $this->googleAnalyticsSessions;
	}
}