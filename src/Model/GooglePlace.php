<?php

namespace DataCollector\Api\Model;

use DateTime;
use JMS\Serializer\Annotation as JMS;

/**
 * Google Place class
 * 
 * @author Marko Curcic <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class GooglePlace
{
    /**
     * ID
     * 
     * @JMS\Type("integer")
     * 
     * @var int
     */
    protected $id;

    /**
     * Name
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $name;
    
    /**
     * Street Number
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $streetNumber;

    /**
     * Street
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $street;

    /**
     * Locality
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $locality;

    /**
     * Administrative Area Level One
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $administrativeAreaLevelOne;

    /**
     * Country
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $country;

    /**
     * Postal Code
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $postalCode;

    /**
     * Formated Phone Number
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $formatedPhoneNumber;

    /**
     * International Phone Number
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $internationalPhoneNumber;

    /**
     * @var string[]
     *
     * @JMS\Type("array<string>")
     */
    protected $types;

    /**
     * Rating
     * 
     * @JMS\Type("integer")
     *
     * @var integer
     */
    protected $rating;

    /**
     * Google Url
     * 
     * @var string
     *
     * @JMS\Type("string")
     */
    protected $google_url;

    /**
     * Website
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $website;

    /**
     * Updated At
     * 
     * @JMS\Type("DateTime")
     *
     * @var DateTime
     */
    protected $updatedAt;

    /**
     * Set id
     *
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set streetNumber
     *
     * @param string $streetNumber
     *
     * @return self
     */
    public function setStreetNumber($streetNumber)
    {
        $this->streetNumber = $streetNumber;

        return $this;
    }

    /**
     * Get streetNumber
     *
     * @return string
     */
    public function getStreetNumber()
    {
        return $this->streetNumber;
    }

        /**
     * Set street
     *
     * @param string $street
     *
     * @return self
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set locality
     *
     * @param string $locality
     *
     * @return self
     */
    public function setLocality($locality)
    {
        $this->locality = $locality;

        return $this;
    }

    /**
     * Get locality
     *
     * @return string
     */
    public function getLocality()
    {
        return $this->locality;
    }

    /**
     * Set administrativeAreaLevelOne
     *
     * @param string $administrativeAreaLevelOne
     *
     * @return self
     */
    public function setAdministrativeAreaLevelOne($administrativeAreaLevelOne)
    {
        $this->administrativeAreaLevelOne = $administrativeAreaLevelOne;

        return $this;
    }

    /**
     * Get administrativeAreaLevelOne
     *
     * @return string
     */
    public function getAdministrativeAreaLevelOne()
    {
        return $this->administrativeAreaLevelOne;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return self
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     *
     * @return self
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set formatedPhoneNumber
     *
     * @param string $formatedPhoneNumber
     *
     * @return self
     */
    public function setFormatedPhoneNumber($formatedPhoneNumber)
    {
        $this->formatedPhoneNumber = $formatedPhoneNumber;

        return $this;
    }

    /**
     * Get formatedPhoneNumber
     *
     * @return string
     */
    public function getFormatedPhoneNumber()
    {
        return $this->formatedPhoneNumber;
    }

    /**
     * Set internationalPhoneNumber
     *
     * @param string $internationalPhoneNumber
     *
     * @return self
     */
    public function setInternationalPhoneNumber($internationalPhoneNumber)
    {
        $this->internationalPhoneNumber = $internationalPhoneNumber;

        return $this;
    }

    /**
     * Get internationalPhoneNumber
     *
     * @return string
     */
    public function getInternationalPhoneNumber()
    {
        return $this->internationalPhoneNumber;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     *
     * @return self
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set types
     *
     * @param string[] $types
     *
     * @return self
     */
    public function setTypes($types)
    {
        $this->types = $types;

        return $this;
    }

    /**
     * Get types
     *
     * @return string[]
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * Set google_url
     *
     * @param string $url
     *
     * @return self
     */
    public function setGoogleUrl($url)
    {
        $this->google_url = $url;

        return $this;
    }

    /**
     * Get google_url
     *
     * @return string
     */
    public function getGoogleUrl()
    {
        return $this->google_url;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return self
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set updatedAt
     *
     * @param DateTime $updatedAt
     *
     * @return self
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
