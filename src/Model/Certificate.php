<?php

namespace DataCollector\Api\Model;

use JMS\Serializer\Annotation as JMS;
/**
 * Certificate class
 * 
 * @author Marko Curcic <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class Certificate
{
    /**
     * @var string
     *
     * @JMS\Type("string")
     */
    protected $name;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Certificate
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}