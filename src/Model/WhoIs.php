<?php

namespace DataCollector\Api\Model;

use DateTime;
use JMS\Serializer\Annotation as JMS;

/**
 * WhoIs class
 * 
 * @author Marko Curcic <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class WhoIs
{
    /**
     * ID
     * 
     * @JMS\Type("integer")
     * 
     * @var int
     */
    protected $id;

    /**
     * Name
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $name;

    /**
     * Organization
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $organization;

    /**
     * Address
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $address;

    /**
     * Zip
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $zip;

    /**
     * City
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $city;

    /**
     * Country
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $country;

    /**
     * IDN Name
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $idn_name;

    /**
     * Created
     * 
     * @JMS\Type("DateTime")
     *
     * @var DateTime
     */
    protected $created;

    /**
     * Changed
     * 
     * @JMS\Type("DateTime")
     *
     * @var DateTime
     */
    protected $changed;

    /**
     * Expires
     * 
     * @JMS\Type("DateTime")
     *
     * @var DateTime
     */
    protected $expires;

    /**
     * WhoIs server
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $whoisserver;

    /**
     * Updated At
     * 
     * @JMS\Type("DateTime")
     *
     * @var DateTime
     */
    protected $updatedAt;

    /**
     * Set id
     *
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set organization
     *
     * @return string
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * Set organization
     *
     * @param string $organization
     *
     * @return self
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return self
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set zip
     *
     * @param string $zip
     *
     * @return self
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return self
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get idn_name
     *
     * @return string
     */
    public function getIdnName()
    {
        return $this->idn_name;
    }

    /**
     * Set idn_name
     *
     * @param string $idn_name
     *
     * @return self
     */
    public function setIdnName($idn_name)
    {
        $this->idn_name = $idn_name;

        return $this;
    }

    /**
     * Get created
     *
     * @return DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed
     *
     * @param DateTime $created
     *
     * @return self
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get changed
     *
     * @return DateTime
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set changed
     *
     * @param DateTime $changed
     *
     * @return self
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;

        return $this;
    }

    /**
     * Get expires
     *
     * @return DateTime
     */
    public function getExpires()
    {
        return $this->expires;
    }

    /**
     * Set expires
     *
     * @param DateTime $expires
     *
     * @return self
     */
    public function setExpires($expires)
    {
        $this->expires = $expires;

        return $this;
    }

    /**
     * Get whoisserver
     *
     * @return string
     */
    public function getWhoisserver()
    {
        return $this->whoisserver;
    }

    /**
     * Set whoisserver
     *
     * @param string $whoisserver
     *
     * @return self
     */
    public function setWhoisserver($whoisserver)
    {
        $this->whoisserver = $whoisserver;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt
     *
     * @param DateTime $updatedAt
     *
     * @return self
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}