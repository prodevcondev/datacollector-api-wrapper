<?php

namespace DataCollector\Api\Model;

use JMS\Serializer\Annotation as JMS;

/**
 * Payment System class
 * 
 * @author Marko Curcic <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class PaymentSystem
{
    /**
     * Name
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $name;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}