<?php 

namespace DataCollector\Api\Model;

use DateTime;
use JMS\Serializer\Annotation as JMS;

/**
 * Google Analytics Path class
 * 
 * @author Marko Curcic <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class GoogleAnalyticsPath
{
	/**
	 * Path
	 * 
	 * @JMS\Type("string")
	 * 
	 * @var string
	 */
	protected $path;

	/**
	 * Session Duraration
	 * 
	 * @JMS\Type("integer")
	 * 
	 * @var int
	 */
	protected $sessionDuration;

	/**
	 * Time
	 * 
	 * @JMS\Type("DateTime")
	 * 
	 * @var DateTime
	 */
	protected $time;

	/**
	 * Set Path
	 *
	 * @param string $path
	 *
	 * @return self
	 */
	public function setPath($path)
	{
		$this->path = $path;
		return $this;
	}

	/**
	 * Get Path
	 *
	 * @return string
	 */
	public function getPath()
	{
		return $this->path;
	}

	/**
	 * Set Sessions Duration
	 *
	 * @param int $duration
	 *
	 * @return self
	 */
	public function setSessionDuration(int $duration)
	{
		$this->sessionDuration = $duration;
		return $this;
	}

	/**
	 * Get Sessions Duration
	 *
	 * @return int
	 */
	public function getSessionDuration()
	{
		return $this->sessionDuration;
	}

	/**
	 * Set Time
	 *
	 * @param DateTime $time
	 *
	 * @return self
	 */
	public function setTime(DateTime $time)
	{
		$this->time = $time;
		return $this;
	}

	/**
	 * Get Time
	 *
	 * @return DateTime $time
	 */
	public function getTime()
	{
		return $this->time;
	}
}