<?php

namespace DataCollector\Api\Model;

use DateTime;
use JMS\Serializer\Annotation as JMS;

/**
 * Website Data class
 * 
 * @author Marko Curcic <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class WebsiteData
{
    /**
     * Score
     * 
     * @JMS\Type("integer")
     * 
     * @var int
     */
    protected $score;

    /**
     * Contact_url
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $contact_url;

    /**
     * Impressum Url
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $impressum_url;

    /**
     * Emails
     * 
     * @JMS\Type("array<string>")
     *
     * @var string[]
     */
    protected $emails;

    /**
     * Company Registration Number
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $companyRegistrationNum;

    /**
     * Company Vat Number
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $companyVatNum;

    /**
     * Phone Number
     * 
     * @JMS\Type("array<string>")
     *
     * @var string[]
     */
    protected $phoneNumbers;

    /**
     * Seo H1 Count
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $seoH1Count;

    /**
     * Seo H2 Count
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $seoH2Count;

    /**
     * Seo H3 Count
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $seoH3Count;

    /**
     * Seo Headline Title Match Score
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $seoHeadlineTitleMatchScore;

    /**
     * Is Webshop
     * 
     * @JMS\Type("bool")
     *
     * @var bool
     */
    protected $isWebshop;

    /**
     * Is Shippment Free
     * 
     * @JMS\Type("bool")
     *
     * @var bool
     */
    protected $isShippmentFree;

    /**
     * Updated At
     * 
     * @JMS\Type("DateTime")
     *
     * @var DateTime
     */
    protected $updatedAt;

    /**
     * Payment Systems
     * 
     * @JMS\Type("array<DataCollector\Api\Model\PaymentSystem>")
     * 
     * @var PaymentSystem[]
     */
    protected $paymentSystems;

    /**
     * Certificates
     * 
     * @JMS\Type("array<DataCollector\Api\Model\Certificate>")
     * 
     * @var Certificate[]
     */
    protected $certificates;

    /**
     * Set score
     *
     * @param int $score
     *
     * @return self
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return int
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set contactUrl
     *
     * @param string $contactUrl
     *
     * @return self
     */
    public function setContactUrl($contactUrl)
    {
        $this->contact_url = $contactUrl;

        return $this;
    }

    /**
     * Get contactUrl
     *
     * @return string
     */
    public function getContactUrl()
    {
        return $this->contact_url;
    }

    /**
     * Set impressumUrl
     *
     * @param string $impressumUrl
     *
     * @return self
     */
    public function setImpressumUrl($impressumUrl)
    {
        $this->impressum_url = $impressumUrl;

        return $this;
    }

    /**
     * Get impressumUrl
     *
     * @return string
     */
    public function getImpressumUrl()
    {
        return $this->impressum_url;
    }

    /**
     * Set emails
     *
     * @param string[] $emails
     *
     * @return self
     */
    public function setEmails($emails)
    {
        $this->emails = $emails;

        return $this;
    }

    /**
     * Get emails
     *
     * @return string[]
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * Set companyRegistrationNum
     *
     * @param string $companyRegistrationNum
     *
     * @return self
     */
    public function setCompanyRegistrationNum($companyRegistrationNum)
    {
        $this->companyRegistrationNum = $companyRegistrationNum;

        return $this;
    }

    /**
     * Get companyRegistrationNum
     *
     * @return string
     */
    public function getCompanyRegistrationNum()
    {
        return $this->companyRegistrationNum;
    }

    /**
     * Set companyVatNum
     *
     * @param string $companyVatNum
     *
     * @return self
     */
    public function setCompanyVatNum($companyVatNum)
    {
        $this->companyVatNum = $companyVatNum;

        return $this;
    }

    /**
     * Get companyVatNum
     *
     * @return string
     */
    public function getCompanyVatNum()
    {
        return $this->companyVatNum;
    }

    /**
     * Set phoneNumbers
     *
     * @param string[] $phoneNumbers
     *
     * @return self
     */
    public function setPhoneNumbers($phoneNumbers)
    {
        $this->phoneNumbers = $phoneNumbers;

        return $this;
    }

    /**
     * Get phoneNumbers
     *
     * @return string[]
     */
    public function getPhoneNumbers()
    {
        return $this->phoneNumbers;
    }

    /**
     * Set seoH1Count
     *
     * @param int $seoH1Count
     *
     * @return self
     */
    public function setSeoH1Count($seoH1Count)
    {
        $this->seoH1Count = $seoH1Count;

        return $this;
    }

    /**
     * Get seoH1Count
     *
     * @return int
     */
    public function getSeoH1Count()
    {
        return $this->seoH1Count;
    }

    /**
     * Set seoH2Count
     *
     * @param int $seoH2Count
     *
     * @return self
     */
    public function setSeoH2Count($seoH2Count)
    {
        $this->seoH2Count = $seoH2Count;

        return $this;
    }

    /**
     * Get seoH2Count
     *
     * @return int
     */
    public function getSeoH2Count()
    {
        return $this->seoH2Count;
    }

    /**
     * Set seoH3Count
     *
     * @param int $seoH3Count
     *
     * @return self
     */
    public function setSeoH3Count($seoH3Count)
    {
        $this->seoH3Count = $seoH3Count;

        return $this;
    }

    /**
     * Get seoH3Count
     *
     * @return int
     */
    public function getSeoH3Count()
    {
        return $this->seoH3Count;
    }

    /**
     * Set seoHeadlineTitleMatchScore
     *
     * @param int $seoHeadlineTitleMatchScore
     *
     * @return self
     */
    public function setSeoHeadlineTitleMatchScore($seoHeadlineTitleMatchScore)
    {
        $this->seoHeadlineTitleMatchScore = $seoHeadlineTitleMatchScore;

        return $this;
    }

    /**
     * Get seoHeadlineTitleMatchScore
     *
     * @return int
     */
    public function getSeoHeadlineTitleMatchScore()
    {
        return $this->seoHeadlineTitleMatchScore;
    }

    /**
     * Set isWebshop
     *
     * @param bool $isWebshop
     *
     * @return self
     */
    public function setIsWebshop($isWebshop)
    {
        $this->isWebshop = $isWebshop;

        return $this;
    }

    /**
     * Get isWebshop
     *
     * @return bool
     */
    public function getIsWebshop()
    {
        return $this->isWebshop;
    }

    /**
     * Set isShippmentFree
     *
     * @param bool $isShippmentFree
     *
     * @return self
     */
    public function setIsShippmentFree($isShippmentFree)
    {
        $this->isShippmentFree = $isShippmentFree;

        return $this;
    }

    /**
     * Get isShippmentFree
     *
     * @return bool
     */
    public function getIsShippmentFree()
    {
        return $this->isShippmentFree;
    }

    /**
     * Set updatedAt
     *
     * @param DateTime $updatedAt
     *
     * @return self
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set paymentSystems
     *
     * @param PaymentSystem[] $paymentSystems
     *
     * @return self
     */
    public function setPaymentSystems($paymentSystems)
    {
        $this->paymentSystems = $paymentSystems;

        return $this;
    }

    /**
     * Get paymentSystems
     *
     * @return PaymentSystem[]
     */
    public function getPaymentSystems() 
    {
        return $this->paymentSystems;
    }

    /**
     * Set certificates
     *
     * @param Certificate[] $certificates
     *
     * @return self
     */
    public function setCertificates($certificates)
    {
        $this->certificates = $certificates;

        return $this;
    }

    /**
     * Get certificates
     *
     * @return Certificate[]
     */
    public function getCertificates()
    {
        return $this->certificates;
    }
}
