<?php 

namespace DataCollector\Api\Model;

use DateTime;
use JMS\Serializer\Annotation as JMS;

/**
 * Kommpass
 * 
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class Kompass
{
    /**
     * Id
     *
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $id;

    /**
     * Name
     *
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $name;

    /**
     * Kompass Id
     *
     * @JMS\Type("integer")
     *
     * @var string
     */
    protected $kompassId;

    /**
     * Language Code
     *
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $languageCode;

    /**
     * Registartion Number
     *
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $registrationNumber;

    /**
     * VAT Number
     *
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $vatNumber;

    /**
     * Phone
     *
     * @JMS\Type("string")
     * 
     * @var string
     */
    protected $phoneNumber;

    /**
     * Fax Number
     *
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $faxNumber;

    /**
     * Email
     *
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $email;

    /**
     * Emails
     *
     * @JMS\Type("array<string>")
     *
     * @var string[]
     */
    protected $emails;

    /**
     * Websites
     *
     * @JMS\Type("array<string>")
     *
     * @var string[]
     */
    protected $websites;

    /**
     * Capital
     *
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $capital;

    /**
     * Activity
     *
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $activity;

    /**
     * Head Quarter
     *
     * @JMS\Type("boolean")
     *
     * @var bool
     */
    protected $headQuarter;

    /**
     * Legal Form
     *
     * @var array<string,string>
     *
     * @JMS\Type("array<string,string>")
     */
    protected $legalForm;

    /**
     * Employees
     *
     * @JMS\Type("array<string,array>")
     *
     * @var array<string,array>
     */
    protected $employees;

    /**
     * Kompass Website Friendly Url
     *
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $kompassWebsiteFriendlyUrl;

    /**
     * Fundation Year
     *
     * @JMS\Type("DateTime")
     *
     * @var DateTime|null
     */
    protected $fundationYear;

    /**
     * Classifications
     *
     * @JMS\Type("array<string,array>")
     *
     * @var array<string,array>
     */
    protected $classifications;

    /**
     * Executives
     *
     * @JMS\Type("array<string,array>")
     *
     * @var array<string,array>
     */
    protected $executives;

    /**
     * Updated At
     *
     * @JMS\Type("DateTime")
     * 
     * @var DateTime
     */
    protected $updatedAt;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set kompassId.
     *
     * @param string $kompassId
     *
     * @return self
     */
    public function setKompassId($kompassId)
    {
        $this->kompassId = $kompassId;

        return $this;
    }

    /**
     * Get kompassId.
     *
     * @return string
     */
    public function getKompassId()
    {
        return $this->kompassId;
    }

    /**
     * Set languageCode.
     *
     * @param string $languageCode
     *
     * @return self
     */
    public function setLanguageCode($languageCode)
    {
        $this->languageCode = $languageCode;

        return $this;
    }

    /**
     * Get languageCode.
     *
     * @return string
     */
    public function getLanguageCode()
    {
        return $this->languageCode;
    }

    /**
     * Set registrationNumber.
     *
     * @param string $registrationNumber
     *
     * @return self
     */
    public function setRegistrationNumber($registrationNumber)
    {
        $this->registrationNumber = $registrationNumber;

        return $this;
    }

    /**
     * Get registrationNumber.
     *
     * @return string
     */
    public function getRegistrationNumber()
    {
        return $this->registrationNumber;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return self
     */
    public function setCompanyName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get Name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set vatNumber.
     *
     * @param string $vatNumber
     *
     * @return self
     */
    public function setVatNumber($vatNumber)
    {
        $this->vatNumber = $vatNumber;

        return $this;
    }

    /**
     * Get vatNumber.
     *
     * @return string
     */
    public function getVatNumber()
    {
        return $this->vatNumber;
    }

    /**
     * Set faxNumber.
     *
     * @param string $faxNumber
     *
     * @return self
     */
    public function setFaxNumber($faxNumber)
    {
        $this->faxNumber = $faxNumber;

        return $this;
    }

    /**
     * Get faxNumber.
     *
     * @return string
     */
    public function getFaxNumber()
    {
        return $this->faxNumber;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set emails.
     *
     * @param string[] $emails
     *
     * @return self
     */
    public function setEmails($emails)
    {
        $this->emails = (array) $emails;

        return $this;
    }

    /**
     * Get emails.
     *
     * @return string[]
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * Set websites.
     *
     * @param string[] $websites
     *
     * @return self
     */
    public function setWebsites($websites)
    {
        $this->websites = (array) $websites;

        return $this;
    }

    /**
     * Get websites.
     *
     * @return string[]
     */
    public function getWebsites()
    {
        return $this->websites;
    }

    /**
     * Get Website
     *
     * @return string|null
     */
    public function getWebsite()
    {
        if (null === $this->websites || empty($this->websites)) {
            return null;
        }

        return current($this->websites);
    }

    /**
     * Set capital.
     *
     * @param string $capital
     *
     * @return self
     */
    public function setCapital($capital)
    {
        $this->capital = $capital;

        return $this;
    }

    /**
     * Get capital.
     *
     * @return string
     */
    public function getCapital()
    {
        return $this->capital;
    }

    /**
     * Set activity.
     *
     * @param string $activity
     *
     * @return self
     */
    public function setActivity($activity)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity.
     *
     * @return string
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Set Head Quarter.
     *
     * @param bool $headQuarter
     *
     * @return self
     */
    public function setHeadQuarter($headQuarter)
    {
        $this->headQuarter = $headQuarter;

        return $this;
    }

    /**
     * Get Head Quarter
     *
     * @return bool
     */
    public function getHeadQuarter()
    {
        return $this->headQuarter;
    }

    /**
     * Set legalForm.
     *
     * @param array<string,string> $legalForm
     *
     * @return self
     */
    public function setLegalForm($legalForm)
    {
        $this->legalForm = (array) $legalForm;

        return $this;
    }

    /**
     * Get legalForm.
     *
     * @return array<string,string>
     */
    public function getLegalForm()
    {
        return $this->legalForm;
    }

    /**
     * Set employees.
     *
     * @param array<string,array> $employees
     *
     * @return self
     */
    public function setEmployees($employees)
    {
        $this->employees = (array) $employees;

        return $this;
    }

    /**
     * Get employees.
     *
     * @return array<string,array>
     */
    public function getEmployees()
    {
        return $this->employees;
    }

    /**
     * Get Company Employees
     *
     * @return mixed
     */
    public function getCompanyEmployees()
    {
        $range = null;

        if (null === $this->employees) {
            return $range;
        }

        foreach ($this->employees as $place) {
            if (! key_exists('type', $place) || $place['type'] !== 'COMPANY') {
                continue;
            } 
            $range = $place['range']['label'];
        }

        return $range;
    }

    /**
     * Set kompassWebsiteFriendlyUrl.
     *
     * @param string $kompassWebsiteFriendlyUrl
     *
     * @return self
     */
    public function setKompassWebsiteFriendlyUrl($kompassWebsiteFriendlyUrl)
    {
        $this->kompassWebsiteFriendlyUrl = $kompassWebsiteFriendlyUrl;

        return $this;
    }

    /**
     * Get kompassWebsiteFriendlyUrl.
     *
     * @return string
     */
    public function getKompassWebsiteFriendlyUrl()
    {
        return $this->kompassWebsiteFriendlyUrl;
    }

    /**
     * Set fundationYear.
     *
     * @param DateTime $fundationYear
     *
     * @return self
     */
    public function setFundationYear($fundationYear)
    {
        if (!$fundationYear instanceof DateTime) {
            $fundationYear = DateTime::createFromFormat('Y', $fundationYear);
        }

        $this->fundationYear = $fundationYear;

        return $this;
    }

    /**
     * Get fundationYear.
     *
     * @return string|null
     */
    public function getFundationYear()
    {
        if (! $this->fundationYear instanceof DateTime) {
            return null;
        }

        return $this->fundationYear->format('Y');
    }

    /**
     * Set updatedAt.
     *
     * @param DateTime $updatedAt
     *
     * @return self
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get phone
     *
     * @return  string
     */ 
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set phone
     *
     * @param string  $phoneNumber  Phone
     *
     * @return self
     */ 
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get classifications
     *
     * @return array<string,array>
     */ 
    public function getClassifications()
    {
        return $this->classifications;
    }

    /**
     * Set classifications
     *
     * @param array<string,array> $classifications Classifications
     *
     * @return self
     */ 
    public function setClassifications($classifications)
    {
        $this->classifications = (array) $classifications;

        return $this;
    }

    /**
     * Get Company Type
     * International Standard Industrial Classification of All Economic Activities, forth version (ISIC4)
     *
     * @return string|null
     */
    public function getCompanyType()
    {
        $isic4 = null;

        if (null === $this->classifications) {
            return $isic4;
        }

        foreach ($this->classifications as $classification) {
            if (!key_exists('type', $classification) || $classification['type'] !== 'ISIC4') {
                continue;
            } 
            $isic4 = $classification['value']['label'];
        }

        return $isic4;
    }

    /**
     * Set executives.
     *
     * @param array<string,array> $executives
     *
     * @return self
     */
    public function setExecutives($executives)
    {
        $this->executives = (array) $executives;

        return $this;
    }

    /**
     * Get executives.
     *
     * @return array<string,array>
     */
    public function getExecutives()
    {
        return $this->executives;
    }
}