<?php 

namespace DataCollector\Api\Model;

use JMS\Serializer\Annotation as JMS;

/**
 * Google Analytics Path class
 * 
 * @author Marko Curcic <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class GoogleAnalyticsSession
{
	/**
	 * Source
	 * 
	 * @JMS\Type("string")
	 * 
	 * @var string
	 */
	protected $source;

	/**
	 * Campaign
	 * 
	 * @JMS\Type("string")
	 * 
	 * @var string
	 */
	protected $campaign;

	/**
	 * Medium
	 * 
	 * @JMS\Type("string")
	 * 
	 * @var string
	 */
	protected $medium;

	/**
	 * Paths
	 * 
	 * @JMS\Type("array<DataCollector\Api\Model\GoogleAnalyticsPath>")
	 * 
	 * @var GoogleAnalyticsPath[]
	 */
	protected $paths;

	/**
	 * Set Source
	 *
	 * @param string $source
	 *
	 * @return self
	 */
	public function setSource($source)
	{
		$this->source = $source;
		return $this;
	}

	/**
	 * Get Source
	 *
	 * @return string
	 */
	public function getSource()
	{
		return $this->source;
	}

	/**
	 * Set Campaign
	 *
	 * @param string $campaign
	 *
	 * @return self
	 */
	public function setCampaign($campaign)
	{
		$this->campaign = $campaign;
		return $this;
	}

	/**
	 * Get Campaign
	 *
	 * @return string
	 */
	public function getCampaign()
	{
		return $this->campaign;
	}

	/**
	 * Set Medium
	 *
	 * @param string $medium
	 *
	 * @return self
	 */
	public function setMedium($medium)
	{
		$this->medium = $medium;
		return $this;
	}

	/**
	 * Get Medium
	 *
	 * @return string
	 */
	public function getMedium()
	{
		return $this->medium;
	}

	/**
	 * Set Paths
	 *
	 * @param GoogleAnalyticsPath[] $paths
	 *
	 * @return self
	 */
	public function setPaths($paths)
	{
		$this->paths = $paths;

		return $this;
	}

	/**
	 * Get Paths
	 *
	 * @return GoogleAnalyticsPath[]
	 */
	public function getPaths()
	{
		return $this->paths;
	}
}