<?php

namespace DataCollector\Api\Model;

use DateTime;
use JMS\Serializer\Annotation as JMS;

/**
 * TelSearch class
 * 
 * @author Marko Curcic <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class TelSearch
{
    /**
     * ID
     * 
     * @JMS\Type("integer")
     *
     * @var integer
     */
    protected $id;

    /**
     * Title
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $title;

    /**
     * Updated
     * 
     * @JMS\Type("DateTime")
     *
     * @var \DateTime
     */
    protected $updated;

    /**
     * Published
     * 
     * @JMS\Type("DateTime")
     *
     * @var DateTime
     */
    protected $published;

    /**
     * Nopromo
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $nopromo;

    /**
     * TelSearch ID
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $telSearchId;

    /**
     * Type
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $type;

    /**
     * Name
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $name;

    /**
     * ORG
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $org;

    /**
     * First Name
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $firstname;

    /**
     * Maidenname
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $maidenname;

    /**
     * Occupation
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $occupation;

    /**
     * Street
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $street;

    /**
     * Street No
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $streetno;

    /**
     * Pobox
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $pobox;

    /**
     * Zip
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $zip;

    /**
     * City
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $city;

    /**
     * Canton
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $canton;

    /**
     * Country
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $country;

    /**
     * Phone
     * 
     * @JMS\Type("array<string>")
     *
     * @var string[]
     */
    protected $phone;

    /**
     * Extra
     * 
     * @JMS\Type("array<string>")
     *
     * @var string[]
     */
    protected $extra;

    /**
     * Links
     * 
     * @JMS\Type("array<string>")
     *
     * @var string[]
     */
    protected $links;

   	/**
     * Updated At
     *  
     * @JMS\Type("DateTime")
     * 
     * @var DateTime
     */
    protected $updatedAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     * 
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set updated
     *
     * @param DateTime $updated
     *
     * @return self
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set published
     *
     * @param DateTime $published
     *
     * @return self
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return DateTime
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set nopromo
     *
     * @param string $nopromo
     *
     * @return self
     */
    public function setNopromo($nopromo)
    {
        $this->nopromo = $nopromo;

        return $this;
    }

    /**
     * Get nopromo
     *
     * @return string
     */
    public function getNopromo()
    {
        return $this->nopromo;
    }

    /**
     * Set telSearchId
     *
     * @param string $telSearchId
     *
     * @return self
     */
    public function setTelSearchId($telSearchId)
    {
        $this->telSearchId = $telSearchId;

        return $this;
    }

    /**
     * Get telSearchId
     *
     * @return string
     */
    public function getTelSearchId()
    {
        return $this->telSearchId;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set org
     *
     * @param string $org
     *
     * @return self
     */
    public function setOrg($org)
    {
        $this->org = $org;

        return $this;
    }

    /**
     * Get org
     *
     * @return string
     */
    public function getOrg()
    {
        return $this->org;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return self
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set maidenname
     *
     * @param string $maidenname
     *
     * @return self
     */
    public function setMaidenname($maidenname)
    {
        $this->maidenname = $maidenname;

        return $this;
    }

    /**
     * Get maidenname
     *
     * @return string
     */
    public function getMaidenname()
    {
        return $this->maidenname;
    }

    /**
     * Set occupation
     *
     * @param string $occupation
     *
     * @return self
     */
    public function setOccupation($occupation)
    {
        $this->occupation = $occupation;

        return $this;
    }

    /**
     * Get occupation
     *
     * @return string
     */
    public function getOccupation()
    {
        return $this->occupation;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return self
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set streetno
     *
     * @param string $streetno
     *
     * @return self
     */
    public function setStreetno($streetno)
    {
        $this->streetno = $streetno;

        return $this;
    }

    /**
     * Get streetno
     *
     * @return string
     */
    public function getStreetno()
    {
        return $this->streetno;
    }

    /**
     * Set pobox
     *
     * @param string $pobox
     *
     * @return self
     */
    public function setPobox($pobox)
    {
        $this->pobox = $pobox;

        return $this;
    }

    /**
     * Get pobox
     *
     * @return string
     */
    public function getPobox()
    {
        return $this->pobox;
    }

    /**
     * Set zip
     *
     * @param string $zip
     *
     * @return self
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set canton
     *
     * @param string $canton
     *
     * @return self
     */
    public function setCanton($canton)
    {
        $this->canton = $canton;

        return $this;
    }

    /**
     * Get canton
     *
     * @return string
     */
    public function getCanton()
    {
        return $this->canton;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return self
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set phone
     *
     * @param string[] $phone
     *
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string[]
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set extra
     *
     * @param string[] $extra
     *
     * @return self
     */
    public function setExtra($extra)
    {
        $this->extra = $extra;

        return $this;
    }

    /**
     * Get extra
     *
     * @return string[]
     */
    public function getExtra()
    {
        return $this->extra;
    }

    /**
     * Set links
     *
     * @param string[] $links
     *
     * @return self
     */
    public function setLinks($links)
    {
        $this->links = $links;

        return $this;
    }

    /**
     * Get links
     *
     * @return string[]
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return self
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}