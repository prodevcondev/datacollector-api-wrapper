<?php

namespace DataCollector\Api\Model;

use DateTime;
use JMS\Serializer\Annotation as JMS;

/**
 * Zefix class
 * 
 * @author Marko Curcic <marko.curcic@prodevcon.ch>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class Zefix
{
    /**
     * ID
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $id;

    /**
     * Name
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $name;

    /**
     * CHID
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $chid;

    /**
     * UUID
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $uid;

    /**
     * Legal Form Id
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $legalFormId;

    /**
     * Capital Nom
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $capitalNom;

    /**
     * Purpose
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $purpose;

    /**
     * Weblink
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $weblink;

    /**
     * Organization Name
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $organizationName;

    /**
     * Street
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $street;

    /**
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $houseNumber;

    /**
     * Town
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $town;

    /**
     * Swiss Zip Code
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $swissZipCode;

    /**
     * Country
     * 
     * @JMS\Type("string")
     *
     * @var string
     */
    protected $country;

    /**
     * Total Found
     * 
     * @JMS\Type("integer")
     *
     * @var int
     */
    protected $totalFound;

    /**
     * UpdatedAt
     * 
     * @JMS\Type("DateTime")
     *
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     * 
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set chid
     *
     * @param string $chid
     *
     * @return self
     */
    public function setChid($chid)
    {
        $this->chid = $chid;

        return $this;
    }

    /**
     * Get chid
     *
     * @return string
     */
    public function getChid()
    {
        return $this->chid;
    }

    /**
     * Set uid
     *
     * @param string $uid
     *
     * @return self
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get uid
     *
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set legalFormId
     *
     * @param int $legalFormId
     *
     * @return self
     */
    public function setLegalFormId($legalFormId)
    {
        $this->legalFormId = $legalFormId;

        return $this;
    }

    /**
     * Get legalFormId
     *
     * @return int
     */
    public function getLegalFormId()
    {
        return $this->legalFormId;
    }

    /**
     * Set capitalNom
     *
     * @param string $capitalNom
     *
     * @return self
     */
    public function setCapitalNom($capitalNom)
    {
        $this->capitalNom = $capitalNom;

        return $this;
    }

    /**
     * Get capitalNom
     *
     * @return string
     */
    public function getCapitalNom()
    {
        return $this->capitalNom;
    }

    /**
     * Set purpose
     *
     * @param string $purpose
     *
     * @return self
     */
    public function setPurpose($purpose)
    {
        $this->purpose = $purpose;

        return $this;
    }

    /**
     * Get purpose
     *
     * @return string
     */
    public function getPurpose()
    {
        return $this->purpose;
    }

    /**
     * Set weblink
     *
     * @param string $weblink
     *
     * @return self
     */
    public function setWeblink($weblink)
    {
        $this->weblink = $weblink;

        return $this;
    }

    /**
     * Get weblink
     *
     * @return string
     */
    public function getWeblink()
    {
        return $this->weblink;
    }

    /**
     * Set organizationName
     *
     * @param string $organizationName
     *
     * @return self
     */
    public function setOrganizationName($organizationName)
    {
        $this->organizationName = $organizationName;

        return $this;
    }

    /**
     * Get organizationName
     *
     * @return string
     */
    public function getOrganizationName()
    {
        return $this->organizationName;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return self
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set houseNumber
     *
     * @param string $houseNumber
     *
     * @return self
     */
    public function setHouseNumber($houseNumber)
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    /**
     * Get houseNumber
     *
     * @return string
     */
    public function getHouseNumber()
    {
        return $this->houseNumber;
    }

    /**
     * Set town
     *
     * @param string $town
     *
     * @return self
     */
    public function setTown($town)
    {
        $this->town = $town;

        return $this;
    }

    /**
     * Get town
     *
     * @return string
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * Set swissZipCode
     *
     * @param int $swissZipCode
     *
     * @return self
     */
    public function setSwissZipCode($swissZipCode)
    {
        $this->swissZipCode = $swissZipCode;

        return $this;
    }

    /**
     * Get swissZipCode
     *
     * @return int
     */
    public function getSwissZipCode()
    {
        return $this->swissZipCode;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return self
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set totalFound
     *
     * @param int $totalFound
     *
     * @return self
     */
    public function setTotalFound($totalFound)
    {
        $this->totalFound = $totalFound;

        return $this;
    }

    /**
     * Get totalFound
     *
     * @return int
     */
    public function getTotalFound()
    {
        return $this->totalFound;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return self
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}